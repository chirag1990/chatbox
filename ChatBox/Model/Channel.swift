//
//  Channel.swift
//  ChatBox
//
//  Created by Chirag Tailor on 08/09/2017.
//  Copyright © 2017 TCTAPPS. All rights reserved.
//

import Foundation

struct Channel {
    public private(set) var channelTitle : String!
    public private(set) var channelDescription : String!
    public private(set) var id : String!
}
